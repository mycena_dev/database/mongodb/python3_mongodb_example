# Environment

---

- Python: ^3.6
- Python lib:
  - pymongo: ^3.8.0
- Mongodb: 5.0
- OS: Linux Ubuntu 18.04
- Docker: 20.10.7
- Docker-compose: 1.23.1



# How to launch MongoDB

---

1. Change the file mode of keyfile.

```shell
$ sudo chmod 400 mongodb/keyfile
```

2. Change the owner setting of keyfile.

```shell
$ sudo chown 999:999 mongodb/keyfile
```

3. Change the terminal path to the `mongodb`

```shell
$ cd mongodb
```

5. Use ```docker-compose up``` to launch all MongoDB containers.

```shell
$ docker-compose up
```

6. Waiting for the output shows the message contain `"Authentication succeeded"`

```shell
localmongo1    | {"t":{"$date":"2022-02-16T11:37:51.238+00:00"},"s":"I",  "c":"ACCESS",   "id":20250,   "ctx":"conn19","msg":"Authentication succeeded","attr":{"mechanism":"SCRAM-SHA-256","speculative":false,"principalName":"__system","authenticationDatabase":"local","remote":"127.0.0.1:50824","extraInfo":{}}}
```

7. Use any tool to connect MongoDB with `mongodb://admin:admin123@localhost:27017,localhost:27018,localhost:27019/?replicaSet=rs0`. Suggest the [MongoDB Compass](https://www.mongodb.com/products/compass)


# Special Example 

The `subscribe_streaming.py` is a blocking script, it will monitor the specified collection of MongoDB.
If the specified collection has any change like insert/update/remove, the `subscribe_streaming.py` will print the message about the change event.

1. Use any terminal to run the `subscribe_streaming.py`.

```shell
$ python3 subscribe_streaming.py
```

2. Launch another terminal to run another insert example like the `insert_data.py`.

```shell
$ python3 insert_data.py
```

3. And the terminal of `subscribe_streaming.py` will show the change message likes the following:

```shell
operationType:  insert
data:  {'measurement_datetime': 1645066191013, 'update_datetime': 1645066195013, 'raw_data': '7fea50fab3bd93d1db7a271d199962abe9ee5c1c07ed0123acc857b65d48c9d798a9e890ef8840fcf9eaa2bdd1c9104b1461a5f3a352cf5913410a9f067e7971601bd3eb377556cbc09b6a9a129a237c2887', 'gateway_mac_address': '00-14-78-ee-19-f8', 'no': 59858, '_id': ObjectId('620db7d38fd4818f1b114849')}
```

# Change the `username` and `password` of MongoDB

---

If you need to change the authentication of MongoDB, you can change the `username` and `password` in the `docker-compose.yml`.

And it needs editor two part of the `docker-compose.yml` file.

1. One is the `{username}` and `{password}` in the test command of the `mongo-setup` service for login.

```yml
  mongo-setup:
    network_mode: host
    container_name: mongo-setup
    image: mongo:5.0
    restart: on-failure
    healthcheck:
      test:
        ["CMD", "mongo","--host","localhost", "-u", {username}, "-p", {password}, "--port","27017","--eval", 'rs.initiate( { _id : "rs0",members: [{ _id: 0, host: "localhost:27017" },{ _id: 1, host: "localhost:27018" },{ _id: 2, host: "localhost:27019" }   ]})' ]
      interval: 10s
```

2. The second is the `{username}` and `{password}` in the environment variables setting of the `mongo1` service.

```yml
    mongo1:
        network_mode: host
        hostname: mongo1
        container_name: localmongo1
        image: mongo:5.0
        environment:
          - MONGO_INITDB_ROOT_USERNAME={username}
          - MONGO_INITDB_ROOT_PASSWORD={password}
```

# Export the MongoDB service.

---

If you need let the MongoDB can be connected from others device which are different IP address.
You need to set a specified IP for the MongoDB.

Like the network figure in the following. 


<div align="center">
<img src="https://gitlab.com/acu_sense/python3_mongodb_example/-/raw/master/mongodb/network.drawio.png" alt="mongodb-network" />
</div>.


If the `192.168.1.101` or the `192.168.1.102` device needs connecting to the MongoDB server at the `192.16.1.100` device, 
the IP setting of `docker-compose.yml` needs to be set `192.168.1.100`.

1. Change the `localhost` to `192.168.1.100` in the `healthcheck` command of the `mongo-setup` service.

@ Before
```yml
  mongo-setup:
    network_mode: host
    container_name: mongo-setup
    image: mongo:5.0
    restart: on-failure
    healthcheck:
      test:
        ["CMD",
         "mongo",
         "--host",
         "localhost",
         "-u",
         "admin",
         "-p",
         "admon123",
         "--port",
         "27017",
         "--eval", 
         'rs.initiate( { _id : "rs0",members: [{ _id: 0, host: "localhost:27017" },{ _id: 1, host: "localhost:27018" },{ _id: 2, host: "localhost:27019" }   ]})' ]
      interval: 10s
```

@ After
```yml
  mongo-setup:
    network_mode: host
    container_name: mongo-setup
    image: mongo:5.0
    restart: on-failure
    healthcheck:
      test:
        ["CMD",
         "mongo",
         "--host",
         "localhost",
         "-u",
         "admin",
         "-p",
         "admon123",
         "--port",
         "27017",
         "--eval", 
         'rs.initiate( { _id : "rs0",members: [{ _id: 0, host: "192.168.1.100:27017" },{ _id: 1, host: "192.168.1.100:27018" },{ _id: 2, host: "192.168.1.100:27019" }   ]})' ]
      interval: 10s
```

# Reference

- https://ithelp.ithome.com.tw/articles/10210166
