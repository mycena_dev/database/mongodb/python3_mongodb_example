#!/usr/bin/python3
from domain.record_data import RecordData


class GSensor(RecordData):
    def __init__(self, no: int, gateway_mac_address: str, update_datetime: int, measurement_datetime: int,
                 raw_data: str,_id: object = None):
        super().__init__(no, gateway_mac_address, update_datetime, measurement_datetime, raw_data, _id)

    def encode_mongo_data(self):
        if self._id is None:
            return {"_type": "g_sensor",
                    "no": self.no,
                    "gateway_mac_address": self.gateway_mac_address,
                    "update_datetime": self.update_datetime,
                    "measurement_datetime": self.measurement_datetime,
                    "raw_data": self.raw_data}
        else:
            return {"_type": "g_sensor",
                    "no": self.no,
                    "gateway_mac_address": self.gateway_mac_address,
                    "update_datetime": self.update_datetime,
                    "measurement_datetime": self.measurement_datetime,
                    "raw_data": self.raw_data,
                    "_id": self._id}

    @staticmethod
    def decode_mongo_data(document):
        assert document["_type"] == "g_sensor"
        return GSensor(document["no"],
                     document["gateway_mac_address"],
                     document["update_datetime"],
                     document["measurement_datetime"],
                     document["raw_data"],
                     document["_id"]
                     )