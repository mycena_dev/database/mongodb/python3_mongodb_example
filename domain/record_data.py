#!/usr/bin/python3

class RecordData:
    def __init__(self, no: int, gateway_mac_address: str, update_datetime: int, measurement_datetime: int,
                 raw_data: str, _id: object = None):
        self.measurement_datetime = measurement_datetime
        self.update_datetime = update_datetime
        self.raw_data = raw_data
        self.gateway_mac_address = gateway_mac_address
        self.no = no
        self._id = _id
