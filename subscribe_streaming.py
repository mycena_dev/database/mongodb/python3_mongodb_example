#!/usr/bin/python3

import pymongo
from domain.ibp import IBP

mongo_client = pymongo.MongoClient(host=['localhost:27017', 'localhost:27018', 'localhost:27019'],
                                   username="admin",
                                   password="admin123",
                                   replicaset='rs0')

use_db = mongo_client.get_database("gateway_db")  # gateway_db is the db name
# use_db = mongo_client["gateway_db"]
# use_db = mongo_client.gateway_db
#  The three lines above are the same.

use_collection = use_db.get_collection("ibp")  # ibp is the collection name like table name
# use_collection = use_db["ibp"]
# use_collection = use_db.ibp
#  The three lines above are the same.

change_stream = use_collection.watch()
for change in change_stream:
    print("operationType: ", change["operationType"])
    print("data: ", IBP.decode_mongo_data(change["fullDocument"]).__dict__)