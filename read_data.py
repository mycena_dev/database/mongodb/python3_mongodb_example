#!/usr/bin/python3

import pymongo
import time
from tqdm import tqdm
from domain.ibp import IBP

mongo_client = pymongo.MongoClient(host=['localhost:27017', 'localhost:27018', 'localhost:27019'],
                                   username="admin",
                                   password="admin123",
                                   replicaset='rs0')

use_db = mongo_client.get_database("gateway_db")  # gateway_db is the db name
# use_db = mongo_client["gateway_db"]
# use_db = mongo_client.gateway_db
#  The three lines above are the same.

use_collection = use_db.get_collection("ibp")  # ibp is the collection name like table name
# use_collection = use_db["ibp"]
# use_collection = use_db.ibp
#  The three lines above are the same.

dataNum = 1000

startTime = time.time()
all_ibp_dics = use_collection.find().limit(dataNum).sort("measurement_datetime")
all_ibp_objects = [IBP.decode_mongo_data(doc) for doc in all_ibp_dics]
for ibp in tqdm(all_ibp_objects):
    print(ibp.__dict__)
print("Total cost time: " + str(time.time() - startTime))
