#!/usr/bin/python3

import pymongo
import random
import time
from tqdm import tqdm, trange
from domain.ibp import IBP
from random import randrange

mongo_client = pymongo.MongoClient(host=['localhost:27017', 'localhost:27018', 'localhost:27019'],
                                   username="admin",
                                   password="admin123",
                                   replicaset='rs0')

use_db = mongo_client.get_database("gateway_db")  # gateway_db is the db name
# use_db = mongo_client["gateway_db"]
# use_db = mongo_client.gateway_db
#  The three lines above are the same.

use_collection = use_db.get_collection("ibp")  # ibp is the collection name like table name
# use_collection = use_db["ibp"]
# use_collection = use_db.ibp
#  The three lines above are the same.

dataNum = 50

mac_address_select_list = ['00-14-78-ee-19-f8', '11-45-e3-20-cc', '00-0a-31-26-7d', '0f-ae-32-b4-dd']
hex_list = '0123456789abcdef'
batch = []

startTime = time.time()
for _ in trange(dataNum):
    no = randrange(100000)
    mac_ddr = mac_address_select_list[randrange(len(mac_address_select_list))]
    update_datetime = round(time.time() * 1000)
    measurement_datetime = round((time.time() - randrange(30)) * 1000)
    raw_data = ''.join([hex_list[randrange(len(hex_list))] for _ in range(randrange(100, 255))])
    new_ibp_data = IBP(no=no,
                       gateway_mac_address=mac_ddr,
                       update_datetime=update_datetime,
                       measurement_datetime=measurement_datetime,
                       raw_data=raw_data)
    batch.append(new_ibp_data.encode_mongo_data())

use_collection.insert_many(batch)
print("Total cost time: " + str(time.time() - startTime))
